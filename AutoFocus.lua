-- AutoFocus!

if AutoFocus then return end

AutoFocus = {}
AutoFocus.is_debug = false

local TIME_DELAY = 0.5
local timeLeft = TIME_DELAY

function AutoFocus.Clear()
  AutoFocus.Settings = {}
  AutoFocus.Settings.id = nil
  AutoFocus.Settings.version = "1.0"
  AutoFocus.Settings.pause = 1
  AutoFocus.Print("autofocus settings cleared.")
end

function AutoFocus.Usage()
  AutoFocus.Print("Usage:")
  AutoFocus.Print("  /autofocus : this help text")
  AutoFocus.Print("  /autofocus set : set autofocus to your current friendly target")
  AutoFocus.Print("  /autofocus clear : clear all current autofocus targets")
  AutoFocus.Print("  /autofocus playpause : play/pause autofocus")
  AutoFocus.Print("")
  AutoFocus.Print("Macros:")
  AutoFocus.Print("  /script AutoFocus.Set() : set autofocus to your current friendly target")
  AutoFocus.Print("  /script AutoFocus.Clear() : clear all current autofocus targets")
  AutoFocus.Print("  /script AutoFocus.PlayPause() : play/pause autofocus")
end

function AutoFocus.SlashHandler(args)
	local cmd, arg = args:match("([A-Za-z0-9]+)[ ]?(.*)")
	cmd = not cmd and "" or cmd:lower()
	
  if (cmd == "set") then
    AutoFocus.Set(arg)
    
  elseif (cmd == "clear") then
    AutoFocus.Clear()
    
  elseif (cmd == "playpause") then
    AutoFocus.PlayPause()
    
  elseif (cmd == "") then
  	AutoFocus.Print("Version " .. AutoFocus.Settings.version .. " by SmaugP")
	  AutoFocus.Print("")
    AutoFocus.Usage()
  	
  else
	  AutoFocus.Print("Unknown command. Type /autofocus for usage information,")
  	
  end
end

function AutoFocus.Init()
  local settings_exist = true
  
  if (AutoFocus.Settings == nil) then
    AutoFocus.Clear()
    settings_exist = false
  end
  
  LibSlash.RegisterSlashCmd("autofocus", AutoFocus.SlashHandler)
  
  AutoFocus.Print("Version " .. AutoFocus.Settings.version .. " by SmaugP loaded.")
  
  if (settings_exist) then
    AutoFocus.Print("Settings found!")
  else
    AutoFocus.Print("No settings found!")
    AutoFocus.Usage()
  end
end


-- set and get targets

function AutoFocus.Set()
  local target = TargetInfo:UnitIsNPC("selffriendlytarget") or TargetInfo:UnitName("selffriendlytarget")
  
	if (not target or target == L"") then
    AutoFocus.Print("autofocus unset.")
	  AutoFocus.Settings.id = nil
	else
    AutoFocus.Print("autofocus set to: " .. WStringToString(target))
	  AutoFocus.Settings.id = target
	  AutoFocus.Settings.pause = 0
	end
  
  return target
end

function AutoFocus.PlayPause()
  if AutoFocus.Settings.pause == 1 then
    AutoFocus.Settings.pause = 0
    AutoFocus.Print("play")
  else
    AutoFocus.Settings.pause = 1
    AutoFocus.Print("pause")
  end
end

function AutoFocus.Get()
  local nick
  
  nick = AutoFocus.Settings.id or nil
  
  if (nick == nil) then
    -- AutoFocus.Error("No autofocus set!")
    return nil
  end
  
  return nick
end


-- actually do stuff

function AutoFocus.OnUpdate(elapsed)
  timeLeft = timeLeft - elapsed
  if (timeLeft > 0 or AutoFocus.Settings.pause == 1) then
    return
  end
  timeLeft = TIME_DELAY
  
  local nick = AutoFocus.Get()
  
  if (nick ~= nil) then
  	AutoFocus.Debug("Assisting " .. WStringToString(nick))
  	AutoFocus.SlashExec("/assist " .. WStringToString(nick))
  end
end


-- execute slash commands

function AutoFocus.SlashExec(cmd)
  if (cmd ~= nil) then
  	AutoFocus.Debug("Slash: " .. cmd)
    SystemData.UserInput.ChatText = towstring(cmd)
    BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
  end
end


-- errors and messages

function AutoFocus.Debug(txt)
	if (AutoFocus.is_debug) then
	  AutoFocus.Print(" ** " .. txt)
	end
end

function AutoFocus.Error(txt)
  AutoFocus.Print("Error: " .. txt)
end

function AutoFocus.Print(txt)
  EA_ChatWindow.Print(towstring("[AutoFocus] " .. txt))
end


