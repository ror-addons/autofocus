<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <UiMod name="AutoFocus" version="1.0" date="23/10/2008">
    
    <Description text="If selected friend change target, your target will be automatic change to it" />
    <Author name="SmaugP" email="smaugp@gmail.com" />
    
    <Dependencies>
      <Dependency name="LibSlash" />
    </Dependencies>
    
    <Files>
      <File name="AutoFocus.lua" />
    </Files>
    
		<SavedVariables>
			<SavedVariable name="AutoFocus.Settings" />
		</SavedVariables>
		
    <OnInitialize>
      <CallFunction name="AutoFocus.Init" />
    </OnInitialize>
    
    <OnUpdate>
      <CallFunction name="AutoFocus.OnUpdate" />
    </OnUpdate>
    
    <OnShutdown/>
    
  </UiMod>
</ModuleFile>
